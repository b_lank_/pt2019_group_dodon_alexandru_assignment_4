package PresentationLayer;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

import BusinessLayer.CompositeProduct;
import BusinessLayer.MenuItem;
import BusinessLayer.Order;
import BusinessLayer.Restaurant;

public class WaiterGraphicalUserInterface extends JPanel {
	private static final long serialVersionUID = 1L;
	private JTable orders = new JTable();
	private JTable menu = new JTable();
	private JTable items = new JTable();
	private JScrollPane sp1 = new JScrollPane(orders);
	private JScrollPane sp2 = new JScrollPane(menu);
	private JScrollPane sp3 = new JScrollPane(items);
	private JLabel l1 = new JLabel("Order id");
	private JTextArea t1 = new JTextArea();
	private JLabel l2 = new JLabel("Table");
	private JTextArea t2 = new JTextArea();
	private JButton create = new JButton("Create new order");
	private JButton add = new JButton("Add item to order");
	private JButton remove = new JButton("Remove item from order");
	private JButton generateBill = new JButton("Generate bill");
	private JLabel lPrice = new JLabel("");
	private JLabel msg = new JLabel("Info: Here you will see contextual information.");
	private Restaurant r;
	private List<MenuItem> current = new ArrayList<MenuItem>();
	
	public WaiterGraphicalUserInterface(Restaurant r) {
		this.r = r;
		
		menu.setModel(createMenuTable(r.getMenu()));
		orders.setModel(createOrdersTable(r.getOrders().keySet()));
		items.setModel(createMenuTable(current));
		
		create.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				int id;
				String table = t2.getText();
				try {
					id = Integer.parseInt(t1.getText());
				} catch(Exception e) {
					msg.setText("Info: Check the id field");
					return;
				}
				
				for (Order i : r.getOrders().keySet()) {
					if (i.getOrderID() == id) {
						msg.setText("Info: The id is already in use");
						return;
					}
				}
				
				if (current.size() <= 0) {
					msg.setText("Info: Cannot create an empty order");
					return;
				}
				
				r.createOrder(id, new Date(), table, current);
				current = new ArrayList<MenuItem>();
				items.setModel(createMenuTable(current));
				orders.setModel(createOrdersTable(r.getOrders().keySet()));
				menu.setModel(createMenuTable(r.getMenu()));
			}
		});

		add.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				int i = menu.getSelectedRow();
				
				if (i < 0 || i > r.getMenu().size()) {
					msg.setText("Info: To add to the order, select a menu item");
					return;
				}
				
				current.add(r.getMenu().get(i));
				
				items.setModel(createMenuTable(current));
				orders.setModel(createOrdersTable(r.getOrders().keySet()));
				menu.setModel(createMenuTable(r.getMenu()));
			}
		});		

		remove.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				int i = items.getSelectedRow();
				
				if (i < 0 || i > current.size()) {
					msg.setText("Info: To remove from the order, select an item from the order");
					return;
				}
				
				current.remove(current.get(i));
				
				items.setModel(createMenuTable(current));
				orders.setModel(createOrdersTable(r.getOrders().keySet()));
				menu.setModel(createMenuTable(r.getMenu()));
			}
		});		
		
		generateBill.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				int i = orders.getSelectedRow();

				items.setModel(createMenuTable(current));
				orders.setModel(createOrdersTable(r.getOrders().keySet()));
				menu.setModel(createMenuTable(r.getMenu()));
				
				if (i < 0 || i > r.getOrders().size()) {
					msg.setText("Info: To generate a bill, select an order");
					return;
				}
				
				ArrayList<Order> l = new ArrayList<>();
				for (Order d : r.getOrders().keySet()) {
					l.add(d);
				}
				r.generateBill(l.get(i));
				msg.setText("Info: Bill generated");
			}
		});
		
		this.setLayout(new GridBagLayout());
		
		GridBagConstraints c;

		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 0;
		c.gridy = 0;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 1;
		c.weighty = 0;
		c.insets = new Insets(10,10,5,5);
		this.add(new JLabel("Orders"), c);

		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 1;
		c.gridy = 0;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 1;
		c.weighty = 0;
		c.insets = new Insets(10,5,5,10);
		this.add(new JLabel("Menu"), c);

		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 0;
		c.gridy = 1;
		c.gridheight = 3;
		c.gridwidth = 1;
		c.weightx = 1;
		c.weighty = 1;
		c.insets = new Insets(5,10,5,5);
		this.add(this.sp1, c);

		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 1;
		c.gridy = 1;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 1;
		c.weighty = 1;
		c.insets = new Insets(5,5,5,10);
		this.add(this.sp2, c);

		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 1;
		c.gridy = 2;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 0;
		c.weighty = 0;
		c.insets = new Insets(5,5,5,10);
		this.add(new JLabel("Current order"), c);

		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 1;
		c.gridy = 3;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 1;
		c.weighty = 1;
		c.insets = new Insets(5,5,5,10);
		this.add(this.sp3, c);

		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 0;
		c.gridy = 4;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 0;
		c.weighty = 0;
		c.insets = new Insets(5,10,5,10);
		this.add(this.l1, c);

		c = new GridBagConstraints();
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 0;
		c.gridy = 5;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 0;
		c.weighty = 0;
		c.insets = new Insets(5,10,5,10);
		this.add(this.t1, c);

		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 0;
		c.gridy = 6;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 0;
		c.weighty = 0;
		c.insets = new Insets(5,10,5,10);
		this.add(this.l2, c);

		c = new GridBagConstraints();
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 0;
		c.gridy = 7;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 0;
		c.weighty = 0;
		c.insets = new Insets(5,10,5,10);
		this.add(this.t2, c);
		
		c = new GridBagConstraints();
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 0;
		c.gridy = 8;
		c.gridheight = 1;
		c.gridwidth = 2;
		c.weightx = 0;
		c.weighty = 0;
		c.insets = new Insets(5,10,10,10);
		this.add(this.msg, c);

		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 1;
		c.gridy = 4;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 0;
		c.weighty = 0;
		c.insets = new Insets(5,5,5,10);
		this.add(this.add, c);

		c = new GridBagConstraints();
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 1;
		c.gridy = 5;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 0;
		c.weighty = 0;
		c.insets = new Insets(5,5,5,10);
		this.add(this.remove, c);

		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 1;
		c.gridy = 6;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 0;
		c.weighty = 0;
		c.insets = new Insets(5,5,5,10);
		this.add(this.create, c);

		c = new GridBagConstraints();
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 1;
		c.gridy = 7;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 0;
		c.weighty = 0;
		c.insets = new Insets(5,5,5,10);
		this.add(this.generateBill, c);
		
	}
	
	private DefaultTableModel createMenuTable(List<MenuItem> list) {
		 DefaultTableModel model = new DefaultTableModel()
			{
				private static final long serialVersionUID = 1L;

				public boolean isCellEditable(int row, int column) 
		        {                
		                return false;               
		        };
		    };
		 String[] header = {"Name", "Description", "Price"};
		 
		 model.setColumnIdentifiers(header);
		 
		 String[] row = new String[3];
		 if (list != null) {
			for (MenuItem i : list) {
				row[0] = i.getName();
				row[1] = i.getDescription();
				row[2] = i.getPrice() + " $";
				model.addRow(row);
			}
		 }

		return model;
	 }
	
	private DefaultTableModel createOrdersTable(Set<Order> list) {
		 DefaultTableModel model = new DefaultTableModel()
			{
				private static final long serialVersionUID = 1L;

				public boolean isCellEditable(int row, int column) 
		        {                
		                return false;               
		        };
		    };
		 String[] header = {"Id", "Data", "Table"};
		 
		 model.setColumnIdentifiers(header);
		 
		 String[] row = new String[3];
		 if (list != null) {
			for (Order i : list) {
				row[0] = i.getOrderID() + "";
				row[1] = DateFormat.getDateInstance().format(i.getDate());
				row[2] = i.getTable();
				model.addRow(row);
			}
		 }

		return model;
	 }
}
