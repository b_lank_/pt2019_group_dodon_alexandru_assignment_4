package PresentationLayer;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

public class ChefGraphicalUserInterface extends JPanel implements Observer{
	private static final long serialVersionUID = 1L;
	List<String> activeOrders = new ArrayList<String>();
	JScrollPane sp = new JScrollPane();
	JButton Complete = new JButton("Complete Top Order");
	
	public ChefGraphicalUserInterface() {		
		this.setLayout(new GridBagLayout());
		
		sp.setViewportView(getListFromOrders());
		
		Complete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {{
					if (activeOrders.size() > 0) {
						activeOrders.remove(0);
					}
					sp.setViewportView(getListFromOrders());
				}
			}
		});
		
		GridBagConstraints c;

		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 0;
		c.gridy = 0;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 1;
		c.weighty = 0.5;
		c.insets = new Insets(10,10,10,10);
		this.add(this.sp, c);
		
		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 0;
		c.gridy = 1;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 0;
		c.weighty = 0;
		c.insets = new Insets(10,10,10,10);
		this.add(this.Complete, c);	
		
		this.setMinimumSize(this.getSize());
	}
	
	public void update(Observable o, Object arg) {
		activeOrders.add((String)arg);
		System.out.println((String)arg);
		sp.setViewportView(getListFromOrders());
	}
	
	private JList<String> getListFromOrders() {
		DefaultListModel<String> res = new DefaultListModel<String>();
		
		Iterator<String> i = activeOrders.iterator(); 
		while (i.hasNext()) {
			String s = i.next();
			res.addElement(s);
		}
		if(res.isEmpty()) {
			res.addElement("No current orders.");
		}
		return new JList<String>(res);
	}
}
