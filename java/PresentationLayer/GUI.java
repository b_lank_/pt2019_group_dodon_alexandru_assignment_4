package PresentationLayer;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;
import javax.swing.JTabbedPane;
import javax.swing.SwingUtilities;

import BusinessLayer.Restaurant;
import DataLayer.RestaurantSerializator;

public class GUI extends JFrame {

	private Restaurant restaurant;

	private JTabbedPane tabbedPane;
	private ChefGraphicalUserInterface chef;
	private AdministratorGraphicalUserInterface admin;
	private waiter;
	
	private GUI() {
		super("Restaurant Management System");
		
		this.setVisible(false);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setLayout(new GridBagLayout());
		
		this.chef = new ChefGraphicalUserInterface();
		this.restaurant = new Restaurant(chef);
		this.admin = new AdministratorGraphicalUserInterface(this.restaurant);
		this.waiter = new WaiterGraphicalUserInterface(this.restaurant);
		
		tabbedPane = new JTabbedPane();
		
	    addWindowListener(new WindowAdapter()
        {
            @Override
            public void windowClosing(WindowEvent e)
            {
            	RestaurantSerializator.serializeRestaurant(restaurant);
                System.out.println("Window Closed");
                e.getWindow().dispose();
            }
        });

		this.tabbedPane.addTab("Administrator view", null, admin, "Administrate the menu.");
		this.tabbedPane.setMnemonicAt(0, KeyEvent.VK_1);
		this.tabbedPane.addTab("Waiter view", null, waiter, "View and manage orders.");
		this.tabbedPane.setMnemonicAt(1, KeyEvent.VK_2);
		this.tabbedPane.addTab("Chef view", null, chef, "View active orders transmitted to the chef.");
		this.tabbedPane.setMnemonicAt(2, KeyEvent.VK_3);
		
		GridBagConstraints c;
		
		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 0;
		c.gridy = 0;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 1;
		c.weighty = 1;
		this.add(this.tabbedPane, c);	
		
		this.pack();
		this.setMinimumSize(this.getSize());
		this.setVisible(true);
	}
	
	public static void main(String[] args) 
	{
		@SuppressWarnings("unused")
		GUI app = new GUI();	
	}
}
