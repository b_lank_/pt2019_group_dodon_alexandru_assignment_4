package PresentationLayer;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

import BusinessLayer.BaseProduct;
import BusinessLayer.CompositeProduct;
import BusinessLayer.MenuItem;
import BusinessLayer.Restaurant;

public class AdministratorGraphicalUserInterface extends JPanel {
	private static final long serialVersionUID = 1L;
	private Restaurant r;
	private JTable menu = new JTable();
	private JTable composition = new JTable();
	private JTable selection = new JTable();
	private JScrollPane sp1;
	private JScrollPane sp2;
	private JScrollPane sp3;

	private JLabel l1 = new JLabel("Name");
	private JTextArea t1 = new JTextArea();
	private JLabel l2 = new JLabel("Description");
	private JTextArea t2 = new JTextArea();
	private JLabel l3 = new JLabel("Price");
	private JTextArea t3 = new JTextArea();
	private JButton add = new JButton("Add menu item");
	private JButton update = new JButton("Edit selected");
	private JButton delete = new JButton("Delete selected");
	private JButton deleteComponent = new JButton("Delete selected component");
	private JButton addComponent = new JButton("Add selected component");
	private JLabel msg = new JLabel("Info: Here you will see contextual information.");
		
	public AdministratorGraphicalUserInterface(final Restaurant r) {
		this.r = r;
		sp1 = new JScrollPane(menu);
		sp2 = new JScrollPane(composition);
		sp3 = new JScrollPane(selection);
		
		menu.setModel(this.createTable(r.getMenu()));
		composition.setModel(this.createTable(null));
		selection.setModel(this.createTable(r.getMenu()));
		
		menu.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent arg0) {
				if (menu.getSelectedRow() > -1) {
					MenuItem i = r.getMenu().get(menu.getSelectedRow());
					if (i instanceof CompositeProduct) {
						composition.setModel(createTable(((CompositeProduct) i).getIngredients()));
						t3.setText("");
					} else {
						composition.setModel(createTable(null));
						t3.setText(i.getPrice() + "");
					}
					t1.setText(i.getName());
					t2.setText(i.getDescription());
				}
			}
		});
		
		add.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String name = t1.getText();
				String desc = t2.getText();
				Double price = null;
				
				try {
					price = Double.parseDouble(t3.getText());
				} catch(Exception e) {
					msg.setText("Info: Check the price field.");
					return;
				}
				if (price == null || price <= 0) {
					msg.setText("Info: Check the price field.");
					return;
				}
				
				r.createMenuItem(name, desc, price);
				menu.setModel(createTable(r.getMenu()));
				selection.setModel(createTable(r.getMenu()));
			}
		});

		update.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String name = t1.getText();
				String desc = t2.getText();
				Double price = null;
								
				int index = menu.getSelectedRow();
				if (index < 0 || index >= r.getMenu().size()) {
					msg.setText("Info: To update, please select a menu item.");
					return;
				}
				
				MenuItem toUpdate = r.getMenu().get(index);
				
				if (toUpdate instanceof CompositeProduct) {
					r.editMenuItem(toUpdate, name, desc, ((CompositeProduct) toUpdate).getIngredients());
				} else {
					try {
						price = Double.parseDouble(t3.getText());
					} catch(Exception e) {
						msg.setText("Info: Check the price field.");
						return;
					}
					if (price == null || price <= 0) {
						msg.setText("Info: Check the price field.");
						return;
					}
					
					r.editMenuItem(toUpdate, name, desc, price);	
				}
				menu.setModel(createTable(r.getMenu()));
				selection.setModel(createTable(r.getMenu()));
			}
		});

		delete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int index = menu.getSelectedRow();
				if (index < 0) {
					msg.setText("Info: To delete, please select a menu item.");
					return;
				}

				r.deleteMenuItem(r.getMenu().get(index));
				menu.setModel(createTable(r.getMenu()));
				selection.setModel(createTable(r.getMenu()));
			}
		});
		
		deleteComponent.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				int iMain = menu.getSelectedRow();
				int iSel = composition.getSelectedRow();
				
				if (iMain < 0) {
					msg.setText("Info: To delete a component, select a menu item.");
					return;
				}
				if (iSel < 0) {
					msg.setText("Info: to delete a component, select a component.");
					return;
				}
				
				MenuItem i = r.getMenu().get(iMain);
				((CompositeProduct)i).removeIngredient(((CompositeProduct)i).getIngredients().get(iSel));
				composition.setModel(createTable(((CompositeProduct)i).getIngredients()));
				menu.setModel(createTable(r.getMenu()));
			}
		});
		
		addComponent.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				int iMain = menu.getSelectedRow();
				int iSel = selection.getSelectedRow();
				
				if (iMain < 0) {
					msg.setText("Info: To add a component, select a menu item.");
					return;
				}
				if (iSel < 0) {
					msg.setText("Info: to add a component, select a component.");
					return;
				}
				if (iMain == iSel) {
					msg.setText("Info: why would you want recursion?");
					return;
				}
				
				MenuItem i = r.getMenu().get(iMain);
				
				if (i instanceof CompositeProduct) {
					((CompositeProduct) i).addIngredient(r.getMenu().get(iSel));
				} else {
					CompositeProduct j = new CompositeProduct((BaseProduct)i, r.getMenu().get(iSel));
					r.deleteMenuItem(i);
					r.getMenu().add(j);
				}
				
				composition.setModel(createTable(null));
				menu.setModel(createTable(r.getMenu()));
			}
		});
		
		setLayout(new GridBagLayout());
		
		GridBagConstraints c;

		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 0;
		c.gridy = 0;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 1;
		c.weighty = 0;
		c.insets = new Insets(10,10,0,5);
		this.add(new JLabel("Menu items"), c);
		
		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 1;
		c.gridy = 0;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 1;
		c.weighty = 0;
		c.insets = new Insets(10,5,0,10);
		this.add(new JLabel("Components"), c);
		
		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 2;
		c.gridy = 0;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 1;
		c.weighty = 0;
		c.insets = new Insets(10,0,0,10);
		this.add(new JLabel("Component selection"), c);
		
		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 0;
		c.gridy = 1;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 0.5;
		c.weighty = 1;
		c.insets = new Insets(10,10,10,5);
		this.add(this.sp1, c);

		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 1;
		c.gridy = 1;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 0.5;
		c.weighty = 1;
		c.insets = new Insets(10,5,10,10);
		this.add(this.sp2, c);

		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 2;
		c.gridy = 1;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 0.5;
		c.weighty = 1;
		c.insets = new Insets(10,0,10,10);
		this.add(this.sp3, c);

		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 0;
		c.gridy = 2;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 1;
		c.weighty = 0;
		c.insets = new Insets(5,10,10,10);
		this.add(this.l1, c);

		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 0;
		c.gridy = 3;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 0.5;
		c.weighty = 0;
		c.insets = new Insets(5,10,10,10);
		this.add(this.t1, c);

		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 0;
		c.gridy = 4;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 1;
		c.weighty = 0;
		c.insets = new Insets(5,10,10,10);
		this.add(this.l2, c);

		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 0;
		c.gridy = 5;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 0.5;
		c.weighty = 0;
		c.insets = new Insets(5,10,10,10);
		this.add(this.t2, c);

		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 0;
		c.gridy = 6;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 1;
		c.weighty = 0;
		c.insets = new Insets(5,10,10,10);
		this.add(this.l3, c);

		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 0;
		c.gridy = 7;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 0.5;
		c.weighty = 0;
		c.insets = new Insets(5,10,10,10);
		this.add(this.t3, c);

		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 1;
		c.gridy = 2;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 0;
		c.weighty = 0;
		c.insets = new Insets(5,10,10,10);
		this.add(this.add, c);

		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 1;
		c.gridy = 4;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 0;
		c.weighty = 0;
		c.insets = new Insets(5,10,10,10);
		this.add(this.update, c);

		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 1;
		c.gridy = 6;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 0;
		c.weighty = 0;
		c.insets = new Insets(5,10,10,10);
		this.add(this.delete, c);

		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 1;
		c.gridy = 8;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 0;
		c.weighty = 0;
		c.insets = new Insets(5,10,10,10);
		this.add(this.deleteComponent, c);

		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 2;
		c.gridy = 2;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 0;
		c.weighty = 0;
		c.insets = new Insets(5,0,10,10);
		this.add(this.addComponent, c);

		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 0;
		c.gridy = 8;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 0;
		c.weighty = 0;
		c.insets = new Insets(5,10,10,10);
		this.add(this.msg, c);
	}
	
	private DefaultTableModel createTable(List<MenuItem> list) {
		 DefaultTableModel model = new DefaultTableModel()
			{
				private static final long serialVersionUID = 1L;

				public boolean isCellEditable(int row, int column) 
		        {                
		                return false;               
		        };
		    };
		 String[] header = {"Name", "Description", "Price"};
		 
		 model.setColumnIdentifiers(header);
		 
		 String[] row = new String[3];
		 if (list != null) {
			for (MenuItem i : list) {
				row[0] = i.getName();
				row[1] = i.getDescription();
				row[2] = i.getPrice() + " $";
				model.addRow(row);
			}
		 }

		return model;
	 }
}
