package DataLayer;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import BusinessLayer.MenuItem;
import BusinessLayer.Order;

public class FileWriter {
	
	public static void CreateBill(Order order, List<MenuItem> items) {
		try {
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd|HH:mm:ss");
			String filename = "bill|"+ dateFormat.format(order.getDate()) + 
					"|orderID:" + order.getOrderID() + "|Table:" + order.getTable() + ".txt";
			File file = new File(filename);
			PrintWriter w = new PrintWriter(file);
			w.println("Bill for order with id " + order.getOrderID() + " at table " + order.getTable());
			w.println("Date of creation: " + dateFormat.format(order.getDate()));
			w.println("\nItems:\n");
			
			double total = 0;
			
			for (MenuItem i: items)
			{	
				w.println(i.getName());
				w.println("    Price: " + i.getPrice());
				total += i.getPrice();
			}
			
			w.println("Total cost: " + total);
			w.close();
		}
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		}
		catch (IOException e) 
		{
			e.printStackTrace();
		}		
	}
}
