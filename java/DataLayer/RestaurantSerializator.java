package DataLayer;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import BusinessLayer.Restaurant;

public class RestaurantSerializator {
	private static String filename = "hw4.restaurant.ser";
	
	public static void serializeRestaurant(Restaurant r) {
        try
        {    
        	FileOutputStream file = new FileOutputStream(filename); 
            ObjectOutputStream out = new ObjectOutputStream(file); 
              
            out.writeObject(r); 
              
            out.close(); 
            file.close(); 
              
            System.out.println("Restaurant has been serialized"); 
  
        }         
        catch(IOException ex) 
        { 
            System.out.println("IOException is caught"); 
        	ex.printStackTrace();
        }  
	}
	
	public static Restaurant deserializeRestaurant() {
		Restaurant r = null;
		try
        {    
            FileInputStream file = new FileInputStream(filename); 
            ObjectInputStream in = new ObjectInputStream(file); 
              
            r = (Restaurant)in.readObject(); 
              
            in.close(); 
            file.close(); 
              
            System.out.println("Restaurant has been deserialized ");
        } 
          
        catch(IOException ex) 
        { 
            System.out.println("IOException is caught"); 
        	ex.printStackTrace();
        } 
          
        catch(ClassNotFoundException ex) 
        { 
            System.out.println("ClassNotFoundException is caught"); 
        	ex.printStackTrace();
        } 
		
		return r;
	}
}
