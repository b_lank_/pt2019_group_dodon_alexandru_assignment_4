package BusinessLayer;

import java.io.Serializable;

public class BaseProduct extends MenuItem {	
	public BaseProduct(String name, String description, double price) {
		super(name, description);
		this.price = price;
	}

	@Override
	public double computePrice() {
		return price;
	}
	
	@Override	
	public double getPrice() {
		return price;
	}
	
	public void setPrice(double price) {
		this.price = price;		
	}
}
