package BusinessLayer;

import java.util.Date;
import java.util.List;

public interface IRestaurantProcessig {

	void createMenuItem(String Name, String description, Double price);
	public void createMenuItem(String Name, String description, List<MenuItem> composites);
	void deleteMenuItem(MenuItem toDelete);
	void editMenuItem(MenuItem toEdit, String name, String description, Double price);
	void editMenuItem(MenuItem toEdit, String name, String description, List<MenuItem> list);
	void createOrder(int id, Date date, String Table, List<MenuItem> items);
	double computeOrderPrice(Order r);
	void generateBill(Order r);
	
}
