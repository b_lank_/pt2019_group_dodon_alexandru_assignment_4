package BusinessLayer;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

import javax.swing.table.DefaultTableModel;

import DataLayer.FileWriter;
import DataLayer.RestaurantSerializator;
import PresentationLayer.ChefGraphicalUserInterface;

public class Restaurant extends Observable implements IRestaurantProcessig, Serializable {

	private static final long serialVersionUID = 1L;
	private Map<Order,List<MenuItem>> orders;
	private List<MenuItem> menu;
	private List<Observer> observers;
	
	public Restaurant(ChefGraphicalUserInterface c) {
		Restaurant r = RestaurantSerializator.deserializeRestaurant();
	
		this.observers = new ArrayList<Observer>();
		observers.add(c);
		
		if (r == null)
		{
			this.orders = new HashMap<Order,List<MenuItem>>();
			this.menu = new ArrayList<MenuItem>();
		} else {
			this.orders = r.getOrders();
			this.menu = r.getMenu();
		}
	}
	
	public Map<Order, List<MenuItem>> getOrders() {
		return orders;
	}

	public void setOrdersgetOrders(HashMap<Order, List<MenuItem>> orders) {
		this.orders = orders;
	}
	
	private boolean isWellFormed() {
		if (this.orders == null || this.menu == null) {
			return false;
		}
		
		boolean ok;
		for (List<MenuItem> i : orders.values()) {
			for (MenuItem j : i) {
				ok = false;
				for (MenuItem item : menu) {
					if (j.equals(item)) {
						ok = true;
						break;
					}
				}
				if (!ok) {
					return false;
				}
			}
		}
		
		return true;
	}

	public List<MenuItem> getMenu() {
		return menu;
	}

	public void setMenu(ArrayList<MenuItem> menu) {
		this.menu = menu;
	}

	public void createMenuItem(String Name, String description, Double price) {
		assert price > 0;
		assert this.isWellFormed();
		
		int preSize = menu.size();
		
		BaseProduct r = new BaseProduct(Name, description, price);
		this.menu.add(r);
		
		assert this.isWellFormed();
		assert preSize == menu.size() + 1;
	}
	
	public void createMenuItem(String Name, String description, List<MenuItem> composites) {
		boolean ok;
		for (MenuItem j : composites) {
			ok = false;
			for (MenuItem item : menu) {
				if (j.equals(item)) {
					ok = true;
					break;
				}
			}
			if (!ok) {
				assert false;
			}
		}

		assert this.isWellFormed();
		
		int preSize = menu.size();
		
		CompositeProduct r = new CompositeProduct(Name, description, composites);
		this.menu.add(r);
		
		assert this.isWellFormed();
		assert preSize == menu.size() + 1;
	}

	public void deleteMenuItem(MenuItem toDelete) {
		assert menu.size() > 0;
		assert toDelete != null;
		assert this.isWellFormed();
		
		int preSize = menu.size();
		
		ArrayList<MenuItem> all = new ArrayList<MenuItem>();
		
		for (MenuItem i : menu) {
			if (i.equals(toDelete)) {
				all.add(i);
			}
			
			if (i instanceof CompositeProduct) {
				for (MenuItem j : ((CompositeProduct)i).getIngredients()) {
					if (j.equals(toDelete)) {
						((CompositeProduct) i).removeIngredient(j);
					}
				}
				
				if (((CompositeProduct) i).getIngredients().size() == 0) {
					all.add(i);
				}
			}
		}
		
		all.forEach(i -> menu.remove(i));
		
		assert this.isWellFormed();
		assert preSize >= menu.size() + 1;
	}

	public void editMenuItem(MenuItem toEdit, String name, String description, Double price) {
		assert price > 0;
		assert toEdit != null;
		assert this.isWellFormed();
		
		int preSize = menu.size();
		
		for (MenuItem i : menu) {
			if (i instanceof BaseProduct && i.equals(toEdit)) {
				i.setName(name);
				i.setDescription(description);
				((BaseProduct)i).setPrice(price);
			}
		}
		
		assert this.isWellFormed();
		assert preSize == menu.size();
	}

	public void editMenuItem(MenuItem toEdit, String name, String description, List<MenuItem> list) {
		assert list != null;
		assert toEdit != null;
		assert this.isWellFormed();
		
		int preSize = menu.size();
		
		for (MenuItem i : menu) {
			if (i instanceof CompositeProduct && i.equals(toEdit)) {
				i.setName(name);
				i.setDescription(description);
				((CompositeProduct)i).setIngredients(list);
			}
		}
		
		assert this.isWellFormed();
		assert preSize == menu.size();
	}

	private String formatOrder(Order r) {
		List<MenuItem> list = orders.get(r);
		
		StringBuilder s = new StringBuilder();
		
		s.append("Id: " + r.getOrderID() + " | Table: " + r.getTable() + " Items: ");
		for (MenuItem i : list) {
			s.append(i.getName() + ", ");
		}
		
		s.delete(s.length() - 2, s.length() - 1);
		
		return s.toString();
	}
	
	public void createOrder(int id, Date date, String Table, List<MenuItem> items) {
		boolean ok;
		for (MenuItem j : items) {
			ok = false;
			for (MenuItem item : menu) {
				if (j.equals(item)) {
					ok = true;
					break;
				}
			}
			if (!ok) {
				assert false;
			}
		}
		
		int preSize = orders.size();
		
		Order r = new Order(id, date, Table);
		
		orders.put(r, items);
		
		String chefOrder = this.formatOrder(r);
		this.notifyObservers(this, chefOrder);
		
		assert this.isWellFormed();
		assert preSize == orders.size() - 1;
	}

	public double computeOrderPrice(Order r) {
		double price = 0;
		List<MenuItem> list = orders.get(r);
		
		for (MenuItem i : list) {
			price += i.getPrice();
		}
		
		return price;
	}

	public void generateBill(Order r) {
		FileWriter.CreateBill(r, orders.get(r));
	}
	

	private void notifyObservers(Observable observable, String order) {
		for(Observer ob : observers) {
			ob.update(observable, order);
		}
	}
}
