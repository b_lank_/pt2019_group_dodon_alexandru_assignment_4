package BusinessLayer;

import java.io.Serializable;

public class Order implements Serializable {
	private static final long serialVersionUID = 1L;
	public int OrderID;
	public java.util.Date Date;
	public String Table;
	
	public Order(int orderID, java.util.Date date, String table) {
		super();
		OrderID = orderID;
		Date = date;
		Table = table;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((Date == null) ? 0 : Date.hashCode());
		result = prime * result + OrderID;
		result = prime * result + ((Table == null) ? 0 : Table.hashCode());
		return prime;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Order other = (Order) obj;
		if (Date == null) {
			if (other.Date != null)
				return false;
		} else if (!Date.equals(other.Date))
			return false;
		if (OrderID != other.OrderID)
			return false;
		if (Table == null) {
			if (other.Table != null)
				return false;
		} else if (!Table.equals(other.Table))
			return false;
		return true;
	}
	
	public int getOrderID() {
		return OrderID;
	}
	
	public void setOrderID(int orderID) {
		OrderID = orderID;
	}
	
	public java.util.Date getDate() {
		return Date;
	}
	
	public void setDate(java.util.Date date) {
		Date = date;
	}
	
	public String getTable() {
		return Table;
	}
	
	public void setTable(String table) {
		Table = table;
	}
	
	
}
