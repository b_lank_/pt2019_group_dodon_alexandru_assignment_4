package BusinessLayer;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class CompositeProduct extends MenuItem {
	private List<MenuItem> ingredients;
	
	public CompositeProduct(String name, String description, List<MenuItem> ingredients) {
		super(name, description);
		this.ingredients = ingredients;
	}

	public CompositeProduct(BaseProduct b, MenuItem ingredient) {
		super(b.getName(), b.getDescription());
		this.ingredients = new ArrayList<MenuItem>();
		ingredients.add(ingredient);
	}
	
	@Override
	public double computePrice() {
		double price = 0;
		
		for (MenuItem i : ingredients)
		{
			price += i.getPrice();
		}
		
		return price + 0.2*price;
	}
	
	@Override
	public double getPrice() {
		return this.computePrice();
	}
	
	public void removeIngredient(MenuItem o) {
		ingredients.remove(o);
	}
	
	public void addIngredient(MenuItem o) {
		ingredients.add(o);
	}

	public List<MenuItem> getIngredients() {
		return ingredients;
	}

	public void setIngredients(List<MenuItem> ingredients) {
		this.ingredients = ingredients;
	}
}
