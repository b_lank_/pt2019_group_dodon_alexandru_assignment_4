package BusinessLayer;

import java.io.Serializable;

public abstract class MenuItem implements Serializable {
	private static final long serialVersionUID = 1L;
	private String name;
	private String description;
	protected double price;
	
	protected MenuItem(String name, String description)
	{
		this.name = name;
		this.description = description;
	}
	
	public abstract double computePrice();
	public abstract double getPrice();
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
}
